package aconsa.server.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataLibraryApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataLibraryApplication.class, args);
	}

}
