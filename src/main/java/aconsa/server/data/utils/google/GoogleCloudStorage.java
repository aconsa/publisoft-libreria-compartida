package aconsa.server.data.utils.google;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

import com.google.api.gax.paging.Page;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.BucketInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GoogleCloudStorage {
    private static final String PROJECT_ID="proyecto-potencia-storage";
    public static final String RESOURCES_PROJECT_ID = "proyecto-potencia-storage";
    public static final String RESOURCES_GOOGLE_AUTH = "./google_auth_potencia.json";
    private static final String BUCKET_NAME="potencia-bucket";
    private final Storage storage;
    private Bucket bucket;
    private String bucketName;

    public GoogleCloudStorage() throws Exception {
        log.debug("Creating new GoogleCloudStorage.");
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream(RESOURCES_GOOGLE_AUTH));
        storage = StorageOptions.newBuilder().setCredentials(credentials).setProjectId(RESOURCES_PROJECT_ID).build().getService();
        bucket = getBucket(BUCKET_NAME);
    }

    // Use path and project name
    public GoogleCloudStorage(String pathToConfig, String projectId, String bucketName) throws IOException {
        log.debug("Creating new GoogleCloudStorage.");
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream(pathToConfig));
        storage = StorageOptions.newBuilder().setCredentials(credentials).setProjectId(projectId).build().getService();
        bucket = getBucket(bucketName);
    }

    // Check for bucket existence and create if needed.
    Bucket getBucket(String bucketName) {
        bucket = storage.get(bucketName);
        if (bucket == null) {
            log.debug("Creating new bucket.");
            bucket = storage.create(BucketInfo.of(bucketName));
        }
        return bucket;
    }

    // Save a string to a blob
    private BlobId saveString(String blobName, String value, Bucket bucket) {
        byte[] bytes = value.getBytes(UTF_8);
        Blob blob = bucket.create(blobName, bytes);
        return blob.getBlobId();
    }

    // Save a string to a blob
    public BlobId saveStringFromArrayByte(String blobName, byte[] value, Bucket bucket) {
        Blob blob = bucket.create(blobName, value);
        return blob.getBlobId();
    }


    // get a blob by id
    private String getString(BlobId blobId) {
        Blob blob = storage.get(blobId);
        return new String(blob.getContent());
    }


    // get a blob by name
    String getContentFileAsString(String name) {
        if(bucket==null)
            bucket = getBucket(bucketName);

        Page<Blob> blobs = bucket.list();
        for (Blob blob: blobs.getValues()) {
            if (name.equals(blob.getName())) {
                return new String(blob.getContent());
            }
        }
        return null;
    }

    public byte[] getContent(String name) {
        Page<Blob> blobs = bucket.list();
        for (Blob blob: blobs.getValues()) {
            if (name.equals(blob.getName())) {
                return blob.getContent();
            }
        }
        return null;
    }

    // Update a blob
    private void updateString(BlobId blobId, String newString) throws IOException {
        Blob blob = storage.get(blobId);
        if (blob != null) {
            WritableByteChannel channel = blob.writer();
            channel.write(ByteBuffer.wrap(newString.getBytes(UTF_8)));
            channel.close();
        }
    }

    public static BlobId saveFileOnGoogleCS(String fileName, byte[] contenfile) throws IOException {
        GoogleCloudStorage googleCloudStorage = new GoogleCloudStorage(RESOURCES_GOOGLE_AUTH, PROJECT_ID, BUCKET_NAME);
        Bucket bucket = googleCloudStorage.getBucket(BUCKET_NAME);
        return googleCloudStorage.saveStringFromArrayByte(fileName,contenfile, bucket);

    }

}
