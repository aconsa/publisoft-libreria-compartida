package aconsa.server.data.utils;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class ErrorResponse {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime timestamp;
    private Integer code;
    private String message;
    private List<String> errors;

    public ErrorResponse(LocalDateTime timestamp, Integer code, String message) {
        this.timestamp = timestamp;
        this.code = code;
        this.message = message;
    }

    public ErrorResponse(LocalDateTime timestamp, Integer code, List<String> errors) {
        this.timestamp = timestamp;
        this.code = code;
        this.errors = errors;
    }
}
