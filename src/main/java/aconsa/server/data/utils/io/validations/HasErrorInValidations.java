package aconsa.server.data.utils.io.validations;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.stream.Collectors;

public class HasErrorInValidations {
    private HasErrorInValidations() throws IllegalAccessException {
        throw new IllegalAccessException("Utility class");
    }

    public static List<String> fetchErrorList(BindingResult result) {
        return result.getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());
    }
}
