package aconsa.server.data.utils.exception;

public class JWTDecodeException extends RuntimeException {
    public JWTDecodeException(String message) {
        super(message);
    }
}
