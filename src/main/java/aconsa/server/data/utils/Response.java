package aconsa.server.data.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class Response {

    public Integer code;
    public String message;
    public Object data;

    public Response() {}

    public Response(String message) {
        this.message = message;
    }

    public Response(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
