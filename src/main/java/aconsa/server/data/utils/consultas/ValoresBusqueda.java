package aconsa.server.data.utils.consultas;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ValoresBusqueda {

    private String codigo;
    private String valor;
}
