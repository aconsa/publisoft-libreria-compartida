package aconsa.server.data.utils.consultas;

import org.apache.commons.lang.StringUtils;

import java.util.List;

public class CrearConsulta {

    private static String sql;
    private static String filtros;

    public static String crearConsultaSql(String nombreTabla, List<ValoresBusqueda> valoresBusqueda) {
        sql = "";
        filtros = "";
        if (!valoresBusqueda.isEmpty()) {
            valoresBusqueda.forEach(item -> {
                filtros += item.getCodigo() + " like '%" + item.getValor() + "%' and ";
            });
            filtros = StringUtils.removeEnd(filtros, " and ");
            sql = "select * from " + nombreTabla + " where " + filtros;
        }
        return sql;
    }
}
