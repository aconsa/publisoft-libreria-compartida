package aconsa.server.data.utils;

public class GeneralMessages {

    public static final String GUARDADO = "El registro de %s se guardo con exito.";
    public static final String ENCONTRADO = "Se ha encontrado el recurso %s";
    public static final String ANULADO = "Se ha anulado el recurso %s";
    public static final String ERROR_AL_GUARDAR = "%s ya existe, no se puede guardar de nuevo.";
    public static final String ERROR_GUARDANDO = "Error al guardar.";

    public static final String ACTUALIZADO = "El registro de %s se actualizó con exito.";
    public static final String ERROR_AL_ACTUALIZAR = "%s no se pudo actualizar.";
    public static final String ERROR_ACTUALIZANDO = "Error al actualizar.";
    public static final String ERROR_ANULANDO = "Error al anular.";

    public static final String ELIMINADO = "El registro de %s se elimino con exito.";
    public static final String ERROR_ELIMINANDO = "Error al eliminar.";

    public static final String ERROR_AL_BUSCAR = "No se encontraron datos guardados de %s.";
    public static final String ERROR_AL_BUSCAR_CON_PARAMETROS = "No se encontraron datos de %s con los parámetros (%s).";

    public static final String REGISTROS_OBTENIDOS_DE_DB = "{} registros obtenidos de la base de datos.";

    public static final String ERROR_500_CON_NOTIFICACIÓN_AUTOMÁTICA = "Error interno del servidor, el equipo de soporte será notificado automáticamente, por favor mantener la calma.";
    public static final String ERROR_500_SIN_NOTIFICACIÓN_AUTOMÁTICA = "Error interno del servidor, comuníquese con el equipo de soporte.";

    public static final String NOTIFICACIÓN_AL_EQUIPO_DE_APOYO = "¡Nuevo error en el recurso %s!.";
    public static final String INFORME_DE_ERRORES = "Nuevo error.";

    public static final String NO_NULL = "no puede ser Nulo.";
    public static final String NO_BLANCO = "no puede estar en Blanco.";
    public static final String TAMAÑO_MINIMO = "debe tener un tamaño Minimo de ";
    public static final String TAMAÑO_MAXIMO = "debe tener un tamaño Maximo de ";

    private GeneralMessages() throws IllegalAccessException {
        throw new IllegalAccessException("Utility class");
    }
}
