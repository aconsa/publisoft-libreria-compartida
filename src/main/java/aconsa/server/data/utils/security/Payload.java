package aconsa.server.data.utils.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Payload implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<String> aud;
    private List<String> scope;
    private Long exp;
    private List<String> authorities;
    private String jti;
    private String client_id;

}
