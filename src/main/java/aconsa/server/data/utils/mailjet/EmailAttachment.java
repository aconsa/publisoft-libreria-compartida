package aconsa.server.data.utils.mailjet;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailAttachment {
    private static final long serialVersionUID = -4800703821707390870L;
    private String contentType;
    private String filename;
    private String base64Content;
}
