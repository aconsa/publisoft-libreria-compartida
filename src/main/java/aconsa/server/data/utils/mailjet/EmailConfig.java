package aconsa.server.data.utils.mailjet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailConfig {
    private String emailTo;
    private String message;
    private String subject;
    private String templateId;
    private Map<String, Object> variables;
    private List<EmailAttachment> attachments;
}

