package aconsa.server.data.domain.generales.genMunicipios;

import aconsa.server.data.utils.GeneralMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GenMunicipios {

    @NotNull(message = "Código sucursal " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código sucursal " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código sucursal " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoSucursal;

    @NotNull(message = "Código país " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código país " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código país " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoPais;

    @NotNull(message = "Código departamento " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código departamento " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código departamento " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoDepartamento;

    @NotNull(message = "Código municipio " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código municipio " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código municipio " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoMunicipio;

    private String descripcionMunicipio;

    @Size(max = 1, message = "Estado municipio " + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String estadoMunicipio;

    private Date fechaCreacion;

    private String usuarioCreacion;

    private Date fechaModificacion;

    private String usuarioModificacion;

}
