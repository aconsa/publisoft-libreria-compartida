package aconsa.server.data.domain.generales.genTipoDocumento;

import aconsa.server.data.utils.GeneralMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GenTipoDocumento {

    @NotNull(message = "Código sucursal " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código sucursal " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código sucursal " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoSucursal;

    @NotNull(message = "Tipo documento " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Tipo documento " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Tipo documento " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String tipoDocumento;

    private String descripcionTipo;

    @Size(max = 1, message = "Estado tipo " + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String estadoTipo;

    private Date fechaCreacion;

    private String usuarioCreacion;

    private Date fechaModificacion;

    private String usuarioModificacion;
}
