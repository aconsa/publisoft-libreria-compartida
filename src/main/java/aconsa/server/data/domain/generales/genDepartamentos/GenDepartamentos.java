package aconsa.server.data.domain.generales.genDepartamentos;

import aconsa.server.data.utils.GeneralMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GenDepartamentos {

    @NotNull(message = "Código sucursal " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código sucursal " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código sucursal " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoSucursal;

    @NotNull(message = "Código país " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código país " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código país " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoPais;

    @NotNull(message = "Código departamento " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código departamento " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código departamento " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoDepartamento;

    private String descripcionDepartamento;

    @Size(max = 1, message = "Estado departamento" + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String estadoDepartamento;

    private Date fechaCreacion;

    private String usuarioCreacion;

    private Date fechaModificacion;

    private String usuarioModificacion;
}
