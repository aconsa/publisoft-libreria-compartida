package aconsa.server.data.domain.contabilidad.conTerceroTipoCuenta;

import aconsa.server.data.utils.GeneralMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ConTerceroTipoCuenta {

    @NotNull(message = "Código sucursal" + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código sucursal" + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código sucursal" + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoSucursal;

    @NotNull(message = "Código tipo " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código tipo " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código tipo " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoTipo;

    private String descripcionTipo;

    private Character estadoTipo;

    private Date fechaCreacion;

    private Date fechaModificacion;

    private String usuarioModificacion;
}
