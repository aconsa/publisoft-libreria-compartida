package aconsa.server.data.domain.contabilidad.conTercero;

import aconsa.server.data.utils.GeneralMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ConTercero {

    @NotNull(message = "Código sucursal" + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código sucursal" + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código sucursal" + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoSucursal;

    @NotNull(message = "Código sucursal" + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código sucursal" + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código sucursal" + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoTercero;

    private String dsctrc;

    private String ap1trc;

    private String ap2trc;

    @Size(max = 2, message = "grstrc" + GeneralMessages.TAMAÑO_MAXIMO + 2)
    private String grstrc;

    @Size(max = 1, message = "rhgtrc" + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String rhgtrc;

    private String ftotrc;

    private String teltrc;

    private String faxtrc;

    private String dirtrc;

    private Date fchtrc;

    @Size(max = 1, message = "sextrc" + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String sextrc;

    @Size(max = 1, message = "tpoemp" + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String tpoemp;

    @Size(max = 50, message = "sgltrc" + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String sgltrc;

    private String crrelc;

    private String cntbnc;

    private Integer clslbr;

    @Size(max = 50, message = "nmrlbr" + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String nmrlbr;

    @Size(max = 50, message = "dstlbr" + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String dstlbr;

    private String ncntrc;

    private Integer grdbsc;

    private String titbsc;

    private Date fchbsc;

    private Integer autrtn;

    @Size(max = 50, message = "cdgofc" + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String cdgofc;

    private String tpotrc;

    @Size(max = 1, message = "esttrc" + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String esttrc;

    private Date fechaCreacion;

    private Date fechaModificacion;

    private String usuarioModificacion;

    private String bnccnt;

    private String tpocnt;

    private String paicrr;

    private String dptcrr;

    private String restrc;

    private String paincm;

    private String dptncm;

    private String munncm;

    private String tpodcm;
}
