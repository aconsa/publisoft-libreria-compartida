package aconsa.server.data.domain.servidorCorreo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Correo {

    private String codigoSucursal;
    private String descripcionSucursal;
    private Long codigoConsecutivo;
    private List<String> destinatarioCorreo;
    private List<String> destinatarioCopia;
    private List<String> destinatarioOculto;
    private String asuntoCorreo;
    private String cuerpoCorreo;
    private String adjuntoCorreo;
    private String nombreArchivoAdjunto;
    private String estadoCorreo;
    private Integer numeroIntentos;
    private Date fechaEnvio;
    private Date fechaCreacion;
    private String usuarioCreacion;
    private Date fechaModificacion;
    private String usuarioModificacion;
}
