package aconsa.server.data.domain.presupuesto.preRubroTipo;

import aconsa.server.data.utils.GeneralMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PreRubroTipo {

    @NotNull(message = "Codigo Sucursal " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Codigo Sucursal " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Codigo Sucursal " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoSucursal;

    @NotNull(message = "Codigo Tipo " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Codigo Tipo " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Codigo Tipo " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoTipo;

    private String descripcionTipo;

    @Size(max = 1, message = "Estado Tipo " + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String estadoTipo;

    private Date fechaCreacion;

    private Date fechaModificacion;

    private String usuarioModificacion;
}
