package aconsa.server.data.domain.presupuesto.preRubroSector;

import aconsa.server.data.utils.GeneralMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PreRubroSector {

    @NotNull(message = "Código sucursal " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código sucursal " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código sucursal " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoSucursal;

    @NotNull(message = "Código sector " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código sector " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código sector " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoSector;

    private String descripcionSector;

    @Size(max = 1, message = "Estado sector " + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String estadoSector;

    private Date fechaCreacion;

    private Date fechaModificacion;

    private String usuarioModificacion;

}
