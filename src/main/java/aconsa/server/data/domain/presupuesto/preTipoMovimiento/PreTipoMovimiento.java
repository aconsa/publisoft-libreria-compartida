package aconsa.server.data.domain.presupuesto.preTipoMovimiento;

import aconsa.server.data.utils.GeneralMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PreTipoMovimiento {

    @NotNull(message = "Codigo Sucursal " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Codigo Sucursal " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Codigo Sucursal " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoSucursal;

    @NotNull(message = "Codigo Tipo " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Codigo Tipo " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Codigo Tipo " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoTipo;

    private String descripcionTipo;

    @Size(max = 2, message = "Cdgafc " + GeneralMessages.TAMAÑO_MAXIMO + 2)
    private String cdgafc;

    private Integer frmafc;

    @Size(max = 1, message = "Clase Tipo " + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String claseTipo;

    @Size(max = 50, message = "Codigo Referencia " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoReferencia;

    private String dtltpo;

    private String ntapie;

    @Size(max = 1, message = "Estado Tipo " + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String estadoTipo;

    private Date fechaCreacion;

    private Date fechaModificacion;

    private String usuarioModificacion;
}
