package aconsa.server.data.domain.presupuesto.preRubro;

import aconsa.server.data.utils.GeneralMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PreRubro {

    @NotNull(message = "Código sucursal " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código sucursal " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código sucursal " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoSucursal;

    @Min(value = 1, message = "Código vigencia " + GeneralMessages.TAMAÑO_MINIMO + 1)
    private Integer codigoVigencia;

    @NotNull(message = "Código rubro " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código rubro " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código rubro " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoRubro;

    private String descripcionRubro;

    @Size(max = 50, message = "cdgfut" + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String cdgfut;

    @Size(max = 50, message = "cdgsct" + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String cdgsct;

    private String rubroActual;

    private String rubroAnterior;

    private String codigoPrograma;

    private String codigoSubPrograma;

    private String codigoElemento;

    private String codigoProducto;

    private String codigoProyecto;

    @Size(max = 50, message = "Cuenta débito " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String cuentaDebito;

    @Size(max = 50, message = "Cuenta crédito " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String cuentaCredito;

    @Size(max = 1, message = "Estado rubro " + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String estadoRubro;

    private Date fechaCreacion;

    private Date fechaModificacion;

    private String usuarioModificacion;

    @Size(max = 1, message = "Tipo rubro " + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String tipoRubro;

    @Size(max = 1, message = "Clase rubro" + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String claseRubro;

    private String codigoSector;
}
