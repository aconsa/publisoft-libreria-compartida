package aconsa.server.data.domain.presupuesto.preTiposMovimientosBases;

import aconsa.server.data.utils.GeneralMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PreTiposMovimientosBases {

    @NotNull(message = "Codigo Sucursal " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Codigo Sucursal " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Codigo Sucursal " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoSucursal;

    @NotNull(message = "Codigo Base " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Codigo Base " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Codigo Base " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoBase;

    @NotNull(message = "Codigo Tipo " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Codigo Tipo " + GeneralMessages.NO_BLANCO)
    @Size(max = 2, message = "Codigo Tipo " + GeneralMessages.TAMAÑO_MAXIMO + 2)
    private String codigoTipo;

    @NotNull(message = "Codigo Afecta " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Codigo Afecta " + GeneralMessages.NO_BLANCO)
    @Size(max = 2, message = "Codigo Afecta " + GeneralMessages.TAMAÑO_MAXIMO + 2)
    private String codigoAfecta;

    private Integer formaAfecta;

    @Size(max = 1, message = "Estado Base " + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String estadoBase;

    private Date fechaCreacion;

    private String usuarioCreacion;

    private Date fechaModificacion;

    private String usuarioModificacion;
}
