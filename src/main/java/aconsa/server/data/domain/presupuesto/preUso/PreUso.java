package aconsa.server.data.domain.presupuesto.preUso;

import aconsa.server.data.utils.GeneralMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PreUso {

    @NotNull(message = "Codigo Sucursal " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Codigo Sucursal " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Codigo Sucursal " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoSucursal;

    @NotNull(message = "Codigo Uso " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Codigo Uso " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Codigo Uso " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoUso;

    private String descripcionUso;

    @Size(max = 1, message = "Estado Uso " + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String estadoUso;

    private Date fechaCreacion;

    private Date fechaModificacion;

    private String usuarioModificacion;
}
