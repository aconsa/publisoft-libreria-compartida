package aconsa.server.data.domain.presupuesto.preRecurso;

import aconsa.server.data.utils.GeneralMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PreRecurso {

    @NotBlank(message = "Código sucursal " + GeneralMessages.NO_BLANCO)
    @NotNull(message = "Código sucursal " + GeneralMessages.NO_NULL)
    @Size(max = 50, message = "Código sucursal " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoSucursal;

    @Min(value = 1, message = "Código vigencia " + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private Integer codigoVigencia;

    @NotBlank(message = "Código recurso " + GeneralMessages.NO_BLANCO)
    @NotNull(message = "Código recurso " + GeneralMessages.NO_NULL)
    @Size(max = 50, message = "Código recurso " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoRecurso;

    private String descripcionRecurso;

    @Size(max = 50, message = "Código banco " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoBanco;

    @Size(max = 1, message = "Estado recurso " + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String estadoRecurso;

    private Date fechaCreacion;

    private Date fechaModificacion;

    private String usuarioModificacion;

}
