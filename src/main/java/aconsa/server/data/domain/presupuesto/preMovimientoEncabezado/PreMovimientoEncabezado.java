package aconsa.server.data.domain.presupuesto.preMovimientoEncabezado;

import aconsa.server.data.utils.GeneralMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PreMovimientoEncabezado {

    @NotBlank(message = "Código sucursal " + GeneralMessages.NO_BLANCO)
    @NotNull(message = "Código sucursal " + GeneralMessages.NO_NULL)
    @Size(max = 50, message = "Código sucursal " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoSucursal;

    @NotBlank(message = "Código movimiento " + GeneralMessages.NO_BLANCO)
    @NotNull(message = "Código movimiento " + GeneralMessages.NO_NULL)
    @Size(max = 50, message = "Código movimiento " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoMovimiento;

    @Size(max = 50, message = "cdgrfr" + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String cdgrfr;

    private Date fechaMovimiento;

    @Size(max = 2000, message = "Descripción movimiento " + GeneralMessages.TAMAÑO_MAXIMO + 2000)
    private String descripcionMovimiento;

    @Size(max = 20, message = "cdgcmp" + GeneralMessages.TAMAÑO_MAXIMO + 20)
    private String cdgcmp;

    private String nmruso;

    private String nombreFirma1;

    private String cargoFirma1;

    private String nombreFirma2;

    private String cargoFirma2;

    @Size(max = 50, message = "Código radicación " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoRadicacion;

    @Size(max = 1, message = "Estado movimiento " + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String estadoMovimiento;

    @Size(max = 1, message = "Firma digital " + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String firmaDigital;

    private Date fechaCreacion;

    private Date fechaModificacion;

    private String usuarioModificacion;

    @Size(max = 50, message = "Código tercero " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoTercero;

    @Size(max = 50, message = "Código secretaria " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoSecretaria;

    @Size(max = 2, message = "Código tipo" + GeneralMessages.TAMAÑO_MAXIMO + 2)
    private String codigoTipo;

    @Size(max = 50, message = "Código uso" + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoUso;

    private String idAdjuntos;

}
