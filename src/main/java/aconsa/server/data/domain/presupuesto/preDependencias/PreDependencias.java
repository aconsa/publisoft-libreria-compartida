package aconsa.server.data.domain.presupuesto.preDependencias;

import aconsa.server.data.utils.GeneralMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PreDependencias {

    @NotNull(message = "Código sucursal " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código sucursal " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código sucursal " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoSucursal;

    @NotNull(message = "Código dependencia " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código dependencia " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código dependencia " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoDependencia;

    private String descripcionDependencia;

    @Size(max = 50, message = "Centro costo" + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String centroCosto;

    private Character estadoDependencia;

    private Date fechaCreacion;

    private String usuarioCreacion;

    private Date fechaModificacion;

    private String usuarioModificacion;
}
