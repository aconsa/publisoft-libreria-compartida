package aconsa.server.data.domain.presupuesto.preMovimientoDetalle;

import aconsa.server.data.utils.GeneralMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PreMovimientoDetalle {

    @NotNull(message = "Código sucursal " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código sucursal " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código sucursal " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoSucursal;

    @NotNull(message = "Código movimiento " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código movimiento " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código movimiento " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoMovimiento;

    @NotNull(message = "cnsrgs " + GeneralMessages.NO_NULL)
    private Integer cnsrgs;

    @DecimalMax(value = "9999999999999999.99", message = "Valor registro " +
            GeneralMessages.TAMAÑO_MAXIMO + 18 + " dígitos")
    private BigDecimal valorRegistro;

    @Size(max = 50, message = "terceroReferencia" + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String terceroReferencia;

    @Size(max = 50, message = "conceptoReferencia" + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String conceptoReferencia;

    @Size(max = 1, message = "estadoRegistro" + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String estadoRegistro;

    private Date fechaCreacion;

    private Date fechaModificacion;

    private String usuarioModificacion;

    @Size(max = 50, message = "cdgrcr" + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String cdgrcr;

    private Integer cdgvgn;

    @Size(max = 50, message = "cdgrbr" + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String cdgrbr;
}
