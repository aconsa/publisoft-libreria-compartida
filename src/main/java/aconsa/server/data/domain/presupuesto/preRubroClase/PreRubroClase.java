package aconsa.server.data.domain.presupuesto.preRubroClase;

import aconsa.server.data.utils.GeneralMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PreRubroClase {

    @NotNull(message = "Código sucursal " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código sucursal " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código sucursal " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoSucursal;

    @NotNull(message = "Código tipo " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código tipo " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código tipo " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoTipo;

    @NotNull(message = "Código clase " + GeneralMessages.NO_NULL)
    @NotBlank(message = "Código clase " + GeneralMessages.NO_BLANCO)
    @Size(max = 50, message = "Código clase " + GeneralMessages.TAMAÑO_MAXIMO + 50)
    private String codigoClase;

    private String descripcionClase;

    private Integer ordenReporte;

    @Size(max = 1, message = "Estado clase " + GeneralMessages.TAMAÑO_MAXIMO + 1)
    private String estadoClase;

    private Date fechaCreacion;

    private Date fechaModificacion;

    private String usuarioModificacion;
}
