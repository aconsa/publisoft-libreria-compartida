package aconsa.server.data.entity.presupuesto.preTiposMovimientosBases;

import aconsa.server.data.domain.presupuesto.preTiposMovimientosBases.PreTiposMovimientosBases;
import aconsa.server.data.entity.presupuesto.preTipoMovimiento.PreTipoMovimientoEntidad;
import aconsa.server.data.entity.presupuesto.preTipoMovimiento.PreTipoMovimientoPK;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "p_tiposmovimientosbases", indexes = @Index(columnList = "formaAfecta, estadoBase"))
public class PreTiposMovimientosBasesEntidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PreTiposMovimientosBasesPK preTiposMovimientosBasesPK;

    @Column(name = "formaAfecta")
    private Integer formaAfecta;

    @Column(name = "estadoBase", length = 1)
    private String estadoBase;

    @Column(name = "fechaCreacion", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "usuarioCreacion")
    private String usuarioCreacion;

    @Column(name = "fechaModificacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Column(name = "usuarioModificacion")
    private String usuarioModificacion;

    @ManyToOne()
    @MapsId("preTiposMovimientosBasesPK")
    @JoinColumns(value = {
            @JoinColumn(name = "codigoSucursal"),
            @JoinColumn(name = "codigoTipo")
    }, foreignKey = @ForeignKey(
            foreignKeyDefinition = "FOREIGN KEY (codigoSucursal, codigoTipo) " +
                    "REFERENCES p_tipomov (cdgscr, cdgtpo) " +
                    "ON UPDATE CASCADE"))
    private PreTipoMovimientoEntidad preTipoMovimiento;

    @ManyToOne()
    @MapsId("preTiposMovimientosBasesPK")
    @JoinColumns(value = {
            @JoinColumn(name = "codigoSucursal"),
            @JoinColumn(name = "codigoAfecta")
    }, foreignKey = @ForeignKey(
            foreignKeyDefinition = "FOREIGN KEY (codigoSucursal, codigoAfecta) " +
                    "REFERENCES p_tipomov (cdgscr, cdgtpo) " +
                    "ON UPDATE CASCADE"))
    private PreTipoMovimientoEntidad preTipoMovimiento1;

    public PreTiposMovimientosBases serialize() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setAmbiguityIgnored(true);
        PreTiposMovimientosBases dto = mapper.map(this, PreTiposMovimientosBases.class);
        if (preTiposMovimientosBasesPK != null) {
            dto.setCodigoSucursal(preTiposMovimientosBasesPK.getCodigoSucursal());
            dto.setCodigoBase(preTiposMovimientosBasesPK.getCodigoBase());
            dto.setCodigoTipo(preTiposMovimientosBasesPK.getCodigoTipo());
            dto.setCodigoAfecta(preTiposMovimientosBasesPK.getCodigoAfecta());
        }
        if (preTipoMovimiento != null && preTipoMovimiento1 != null) {
            dto.setCodigoTipo(preTipoMovimiento.getPreTipoMovimientoPK().getCodigoTipo());
            dto.setCodigoAfecta(preTipoMovimiento1.getPreTipoMovimientoPK().getCodigoTipo());
        }
        return dto;
    }

    public static PreTiposMovimientosBasesEntidad crearEntidadTiposMovimientosBases(PreTiposMovimientosBases preTiposMovimientosBases) {
        PreTiposMovimientosBasesEntidad entidad = new ModelMapper().map(preTiposMovimientosBases, PreTiposMovimientosBasesEntidad.class);
        entidad.setPreTiposMovimientosBasesPK(new PreTiposMovimientosBasesPK(preTiposMovimientosBases.getCodigoSucursal(), preTiposMovimientosBases.getCodigoBase(), preTiposMovimientosBases.getCodigoTipo(), preTiposMovimientosBases.getCodigoAfecta()));
        entidad.setPreTipoMovimiento(new PreTipoMovimientoEntidad(new PreTipoMovimientoPK(preTiposMovimientosBases.getCodigoSucursal(), preTiposMovimientosBases.getCodigoTipo())));
        entidad.setPreTipoMovimiento1(new PreTipoMovimientoEntidad(new PreTipoMovimientoPK(preTiposMovimientosBases.getCodigoSucursal(), preTiposMovimientosBases.getCodigoTipo())));
        return entidad;
    }
}
