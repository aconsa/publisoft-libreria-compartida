package aconsa.server.data.entity.presupuesto.preMovimientoDetalle;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class PreMovimientoDetallePK implements Serializable {

    @Column(name = "cdgscr", columnDefinition = "varchar(50) default ''")
    private String codigoSucursal;

    @Column(name = "cdgmvm", columnDefinition = "varchar(50) default ''")
    private String codigoMovimiento;

    @Column(name = "cnsrgs", columnDefinition = "int(11) default 0")
    private Integer cnsrgs;

}
