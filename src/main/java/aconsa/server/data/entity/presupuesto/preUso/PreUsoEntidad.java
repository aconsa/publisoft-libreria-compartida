package aconsa.server.data.entity.presupuesto.preUso;

import aconsa.server.data.domain.presupuesto.preUso.PreUso;
import aconsa.server.data.entity.presupuesto.preMovimientoEncabezado.PreMovimientoEncabezadoEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "p_uso", indexes = @Index(columnList = "dscuso, estuso"))
public class PreUsoEntidad implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    protected PreUsoPK preUsoPK;

    @Column(name = "dscuso")
    private String descripcionUso;

    @Column(name = "estuso", length = 1)
    private String estadoUso;

    @Column(name = "fchcrc", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "fchmdf")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Column(name = "usrmdf")
    private String usuarioModificacion;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "preUso")
    private List<PreMovimientoEncabezadoEntidad> preMovimientoEncabezado;

    public PreUsoEntidad(PreUsoPK preUsoPK) {
        this.preUsoPK = preUsoPK;
    }

    public PreUso serialize() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setAmbiguityIgnored(true);
        PreUso dto = mapper.map(this, PreUso.class);
        if (preUsoPK != null) {
            dto.setCodigoSucursal(preUsoPK.getCodigoSucursal());
            dto.setCodigoUso(preUsoPK.getCodigoUso());
        }
        return dto;
    }

    public static PreUsoEntidad crearEntidadUso(PreUso preUso) {
        PreUsoEntidad entidad = new ModelMapper().map(preUso, PreUsoEntidad.class);
        entidad.setPreUsoPK(new PreUsoPK(preUso.getCodigoSucursal(), preUso.getCodigoUso()));
        return entidad;
    }
}
