package aconsa.server.data.entity.presupuesto.preRecurso;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class PreRecursoPK implements Serializable {

    @Column(name = "cdgscr", columnDefinition = "varchar(50) default ''")
    private String codigoSucursal;

    @Column(name = "cdgvgn")
    private Integer codigoVigencia;

    @Column(name = "cdgrcr", columnDefinition = "varchar(50) default ''")
    private String codigoRecurso;
}
