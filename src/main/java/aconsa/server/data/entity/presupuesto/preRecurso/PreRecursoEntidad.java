package aconsa.server.data.entity.presupuesto.preRecurso;

import aconsa.server.data.domain.presupuesto.preRecurso.PreRecurso;
import aconsa.server.data.entity.presupuesto.preMovimientoDetalle.PreMovimientoDetalleEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "p_recurso", indexes = @Index(columnList = "dscrcr, estrcr"))
public class PreRecursoEntidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PreRecursoPK preRecursoPK;

    @Column(name = "dscrcr")
    private String descripcionRecurso;

    @Column(name = "codigoBanco", length = 50)
    private String codigoBanco;

    @Column(name = "estrcr", length = 1)
    private String estadoRecurso;

    @Column(name = "fchcrc", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "fchmdf")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Column(name = "usrmdf")
    private String usuarioModificacion;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "preRecurso")
    private List<PreMovimientoDetalleEntidad> preMovimientoDetalle;

    public PreRecursoEntidad(PreRecursoPK preRecursoPK) {
        this.preRecursoPK = preRecursoPK;
    }

    public PreRecurso serialize() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setAmbiguityIgnored(true);
        PreRecurso dto = mapper.map(this, PreRecurso.class);
        if (preRecursoPK != null) {
            dto.setCodigoSucursal(preRecursoPK.getCodigoSucursal());
            dto.setCodigoVigencia(preRecursoPK.getCodigoVigencia());
            dto.setCodigoRecurso(preRecursoPK.getCodigoRecurso());
        }
        return dto;
    }

    public static PreRecursoEntidad crearEntidadRecurso(PreRecurso preRecurso) {
        PreRecursoEntidad entidad = new ModelMapper().map(preRecurso, PreRecursoEntidad.class);
        entidad.setPreRecursoPK(new PreRecursoPK(preRecurso.getCodigoSucursal(), preRecurso.getCodigoVigencia(), preRecurso.getCodigoRecurso()));
        return entidad;
    }
}
