package aconsa.server.data.entity.presupuesto.preTipoMovimiento;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class PreTipoMovimientoPK implements Serializable {

    @Column(name = "cdgscr", columnDefinition = "varchar(50) default ''")
    private String codigoSucursal;

    @Column(name = "cdgtpo", columnDefinition = "varchar(2) default ''")
    private String codigoTipo;

}
