package aconsa.server.data.entity.presupuesto.preDependencias;

import aconsa.server.data.domain.presupuesto.preDependencias.PreDependencias;
import aconsa.server.data.entity.presupuesto.preMovimientoEncabezado.PreMovimientoEncabezadoEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "p_dependencias")
public class PreDependenciasEntidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PreDependenciasPK preDependenciasPK;

    @Column(name = "descripcionDependencia")
    private String descripcionDependencia;

    @Column(name = "centroCosto", length = 50)
    private String centroCosto;

    @Column(name = "estadoDependencia", length = 1, columnDefinition = "char(1) default 'T'")
    private Character estadoDependencia;

    @Column(name = "fechaCreacion", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "usuarioCreacion", updatable = false)
    private String usuarioCreacion;

    @Column(name = "fechaModificacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Column(name = "usuarioModificacion")
    private String usuarioModificacion;

    //@NotFound(action = NotFoundAction.IGNORE)
    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "preDependencias")
    private List<PreMovimientoEncabezadoEntidad> preMovimientoEncabezado;

    public PreDependenciasEntidad(PreDependenciasPK preDependenciasPK) {
        this.preDependenciasPK = preDependenciasPK;
    }

    public PreDependencias serialize() {
        PreDependencias dto = new ModelMapper().map(this, PreDependencias.class);
        if (preDependenciasPK != null) {
            dto.setCodigoSucursal(preDependenciasPK.getCodigoSucursal());
            dto.setCodigoDependencia(preDependenciasPK.getCodigoDependencia());
        }
        return dto;
    }

    public static PreDependenciasEntidad crearPreDependenciasEntidad(PreDependencias preDependencias) {
        PreDependenciasEntidad entidad = new ModelMapper().map(preDependencias, PreDependenciasEntidad.class);
        entidad.setPreDependenciasPK(new PreDependenciasPK(
                preDependencias.getCodigoSucursal(), preDependencias.getCodigoDependencia()
        ));
        return entidad;
    }

}
