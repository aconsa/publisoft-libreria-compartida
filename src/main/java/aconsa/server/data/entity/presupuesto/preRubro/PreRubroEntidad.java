package aconsa.server.data.entity.presupuesto.preRubro;

import aconsa.server.data.domain.presupuesto.preRubro.PreRubro;
import aconsa.server.data.entity.presupuesto.preMovimientoDetalle.PreMovimientoDetalleEntidad;
import aconsa.server.data.entity.presupuesto.preRubroClase.PreRubroClaseEntidad;
import aconsa.server.data.entity.presupuesto.preRubroClase.PreRubroClasePK;
import aconsa.server.data.entity.presupuesto.preRubroSector.PreRubroSectorEntidad;
import aconsa.server.data.entity.presupuesto.preRubroSector.PreRubroSectorPK;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "p_rubro", indexes = {@Index(columnList = "dscrbr, tporbr, clsrbr, estrbr")})
public class PreRubroEntidad {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PreRubroPK preRubroPK;

    @Column(name = "dscrbr")
    private String descripcionRubro;

    @Column(name = "cdgfut", length = 50)
    private String cdgfut;

    @Column(name = "cdgsct", columnDefinition = "varchar(50) default '00'")
    private String cdgsct;

    @Column(name = "rubroActual")
    private String rubroActual;

    @Column(name = "rubroAnterior")
    private String rubroAnterior;

    @Column(name = "codigoPrograma")
    private String codigoPrograma;

    @Column(name = "codigoSubPrograma")
    private String codigoSubPrograma;

    @Column(name = "codigoElemento")
    private String codigoElemento;

    @Column(name = "codigoProducto")
    private String codigoProducto;

    @Column(name = "codigoProyecto")
    private String codigoProyecto;

    @Column(name = "cuentaDebito", length = 50)
    private String cuentaDebito;

    @Column(name = "cuentaCredito", length = 50)
    private String cuentaCredito;

    @Column(name = "estrbr", length = 1)
    private String estadoRubro;

    @Column(name = "fchcrc", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "fchmdf")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Column(name = "usrmdf")
    private String usuarioModificacion;

    @Column(name = "clsrbr", columnDefinition = "varchar(1)")
    private String claseRubro;

    @Column(name = "tporbr", columnDefinition = "varchar(1)")
    private String tipoRubro;

    @Column(name = "codigoSector", columnDefinition = "varchar(255)")
    private String codigoSector;

    @ManyToOne()
    @MapsId("preRubroPK")
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumns(value = {
            @JoinColumn(name = "clsrbr"),
            @JoinColumn(name = "cdgscr"),
            @JoinColumn(name = "tporbr")
    }, foreignKey = @ForeignKey(
            foreignKeyDefinition = "FOREIGN KEY (clsrbr, cdgscr, tporbr) " +
                    "REFERENCES p_rubroclase (cdgcls, cdgscr, cdgtpo) " +
                    "ON UPDATE CASCADE"))
    private PreRubroClaseEntidad preRubroClase;

    @ManyToOne()
    @MapsId("preRubroPK")
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumns(value = {
            @JoinColumn(name = "codigoSector"),
            @JoinColumn(name = "cdgscr")
    }, foreignKey = @ForeignKey(
            foreignKeyDefinition = "FOREIGN KEY (codigoSector, cdgscr) " +
                    "REFERENCES p_rubrosector (cdgsct, cdgscr) " +
                    "ON UPDATE CASCADE"))
    private PreRubroSectorEntidad preRubroSector;

    @NotFound(action = NotFoundAction.IGNORE)
    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "preRubro")
    private List<PreMovimientoDetalleEntidad> preMovimientoDetalle;

    public PreRubroEntidad(PreRubroPK preRubroPK) {
        this.preRubroPK = preRubroPK;
    }

    public PreRubro serialize() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setAmbiguityIgnored(true);
        PreRubro dto = mapper.map(this, PreRubro.class);
        if (preRubroPK != null) {
            dto.setCodigoSucursal(preRubroPK.getCodigoSucursal());
            dto.setCodigoVigencia(preRubroPK.getCodigoVigencia());
            dto.setCodigoRubro(preRubroPK.getCodigoRubro());
        }
        if (preRubroClase != null && preRubroSector != null) {
            dto.setClaseRubro(preRubroClase.getPreRubroClasePK().getCodigoClase());
            dto.setTipoRubro(preRubroClase.getPreRubroClasePK().getCodigoTipo());
            dto.setCodigoSector(preRubroSector.getPreRubroSectorPK().getCodigoSector());
        }
        return dto;
    }

    public static PreRubroEntidad crearEntidadRubro(PreRubro preRubro) {
        PreRubroEntidad entidad = new ModelMapper().map(preRubro, PreRubroEntidad.class);
        entidad.setPreRubroPK(new PreRubroPK(preRubro.getCodigoSucursal(), preRubro.getCodigoVigencia(), preRubro.getCodigoRubro()));
        entidad.setPreRubroClase(new PreRubroClaseEntidad(new PreRubroClasePK(preRubro.getCodigoSucursal(), preRubro.getTipoRubro(), preRubro.getClaseRubro())));
        entidad.setPreRubroSector(new PreRubroSectorEntidad(new PreRubroSectorPK(preRubro.getCodigoSucursal(), preRubro.getCodigoSector())));
        return entidad;
    }
}
