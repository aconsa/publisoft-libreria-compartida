package aconsa.server.data.entity.presupuesto.preRubroSector;

import aconsa.server.data.domain.presupuesto.preRubroSector.PreRubroSector;
import aconsa.server.data.entity.presupuesto.preRubro.PreRubroEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "p_rubrosector", indexes = @Index(columnList = "estsct"))
public class PreRubroSectorEntidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PreRubroSectorPK preRubroSectorPK;

    @Column(name = "dscsct")
    private String descripcionSector;

    @Column(name = "estsct", length = 1)
    private String estadoSector;

    @Column(name = "fchcrc", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "fchmdf")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Column(name = "usrmdf")
    private String usuarioModificacion;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "preRubroSector")
    private List<PreRubroEntidad> preRubro;

    public PreRubroSectorEntidad(PreRubroSectorPK preRubroSectorPK) {
        this.preRubroSectorPK = preRubroSectorPK;
    }

    public PreRubroSector serialize() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setAmbiguityIgnored(true);
        PreRubroSector dto = mapper.map(this, PreRubroSector.class);
        if (preRubroSectorPK != null) {
            dto.setCodigoSucursal(preRubroSectorPK.getCodigoSucursal());
            dto.setCodigoSector(preRubroSectorPK.getCodigoSector());
        }
        return dto;
    }

    public static PreRubroSectorEntidad crearEntidadRubroSector(PreRubroSector preRubroSector) {
        PreRubroSectorEntidad entidad = new ModelMapper().map(preRubroSector, PreRubroSectorEntidad.class);
        entidad.setPreRubroSectorPK(new PreRubroSectorPK(preRubroSector.getCodigoSucursal(), preRubroSector.getCodigoSector()));
        return entidad;
    }

}
