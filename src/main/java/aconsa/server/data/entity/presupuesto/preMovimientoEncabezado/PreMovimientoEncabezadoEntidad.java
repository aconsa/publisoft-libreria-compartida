package aconsa.server.data.entity.presupuesto.preMovimientoEncabezado;

import aconsa.server.data.domain.presupuesto.preMovimientoEncabezado.PreMovimientoEncabezado;
import aconsa.server.data.entity.contabilidad.conTercero.ConTerceroEntidad;
import aconsa.server.data.entity.contabilidad.conTercero.ConTerceroPK;
import aconsa.server.data.entity.presupuesto.preDependencias.PreDependenciasEntidad;
import aconsa.server.data.entity.presupuesto.preDependencias.PreDependenciasPK;
import aconsa.server.data.entity.presupuesto.preMovimientoDetalle.PreMovimientoDetalleEntidad;
import aconsa.server.data.entity.presupuesto.preTipoMovimiento.PreTipoMovimientoEntidad;
import aconsa.server.data.entity.presupuesto.preTipoMovimiento.PreTipoMovimientoPK;
import aconsa.server.data.entity.presupuesto.preUso.PreUsoEntidad;
import aconsa.server.data.entity.presupuesto.preUso.PreUsoPK;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "p_movienc", indexes = {
        @Index(name = "grupoIndexes1",
                columnList = "cdgrfr, cdgtrc, fchmvm, estmvm, cdgtpo"),
        @Index(name = "grupoIndexes2",
                columnList = "cdgscr, cdgrfr")})
public class PreMovimientoEncabezadoEntidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PreMovimientoEncabezadoPK preMovimientoEncabezadoPK;

    @Column(name = "cdgrfr", length = 50)
    private String cdgrfr;

    @Column(name = "fchmvm")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaMovimiento;

    @Column(name = "dscmvm", length = 2000)
    private String descripcionMovimiento;

    @Column(name = "cdgcmp", length = 20)
    private String cdgcmp;

    @Column(name = "nmruso")
    private String nmruso;

    @Column(name = "nombreFirma1")
    private String nombreFirma1;

    @Column(name = "cargoFirma1")
    private String cargoFirma1;

    @Column(name = "nombreFirma2")
    private String nombreFirma2;

    @Column(name = "cargoFirma2")
    private String cargoFirma2;

    @Column(name = "codigoRadicacion", length = 50)
    private String codigoRadicacion;

    @Column(name = "estmvm", length = 1)
    private String estadoMovimiento;

    @Column(name = "firmaDigital", columnDefinition = "varchar(1) default 'N'")
    private String firmaDigital;

    @Column(name = "fchcrc", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "fchmdf")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Column(name = "usrmdf")
    private String usuarioModificacion;

    @Column(name = "cdgtrc", columnDefinition = "varchar(50)")
    private String codigoTercero;

    @Column(name = "codigoSecretaria", columnDefinition = "varchar(50)")
    private String codigoSecretaria;

    @Column(name = "cdgtpo")
    private String codigoTipo;

    @Column(name = "cdguso")
    private String codigoUso;

    @Column(name = "idAdjuntos")
    private String idAdjuntos;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "preMovimientoEncabezado")
    private List<PreMovimientoDetalleEntidad> preMovimientoDetalle;

    @ManyToOne()
    @MapsId("preMovimientoEncabezadoPK")
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumns(value = {
            @JoinColumn(name = "cdgscr"),
            @JoinColumn(name = "cdgtrc")
    }, foreignKey = @ForeignKey(
            foreignKeyDefinition = "FOREIGN KEY (cdgscr, cdgtrc) " +
                    "REFERENCES c_tercero (cdgscr, cdgtrc) " +
                    "ON UPDATE CASCADE"))
    private ConTerceroEntidad conTercero;

    @ManyToOne()
    @MapsId("preMovimientoEncabezadoPK")
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumns(value = {
            @JoinColumn(name = "codigoSecretaria"),
            @JoinColumn(name = "cdgscr")
    }, foreignKey = @ForeignKey(
            foreignKeyDefinition = "FOREIGN KEY (codigoSecretaria, cdgscr) " +
                    "REFERENCES p_dependencias (codigoDependencia, codigoSucursal) " +
                    "ON UPDATE CASCADE"))
    private PreDependenciasEntidad preDependencias;

    @ManyToOne()
    @MapsId("preMovimientoEncabezadoPK")
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumns(value = {
            @JoinColumn(name = "cdgscr"),
            @JoinColumn(name = "cdgtpo")
    }, foreignKey = @ForeignKey(
            foreignKeyDefinition = "FOREIGN KEY (cdgscr, cdgtpo) " +
                    "REFERENCES p_tipomov (cdgscr, cdgtpo) " +
                    "ON UPDATE CASCADE"))
    private PreTipoMovimientoEntidad preTipoMovimiento;

    @ManyToOne()
    @MapsId("preMovimientoEncabezadoPK")
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumns(value = {
            @JoinColumn(name = "cdgscr"),
            @JoinColumn(name = "cdguso")
    }, foreignKey = @ForeignKey(
            foreignKeyDefinition = "FOREIGN KEY (cdgscr, cdguso) " +
                    "REFERENCES p_uso (cdgscr, cdguso) " +
                    "ON UPDATE CASCADE"))
    private PreUsoEntidad preUso;

    public PreMovimientoEncabezadoEntidad(PreMovimientoEncabezadoPK preMovimientoEncabezadoPK) {
        this.preMovimientoEncabezadoPK = preMovimientoEncabezadoPK;
    }

    public PreMovimientoEncabezado serialize() {
        PreMovimientoEncabezado dto = new ModelMapper().map(this, PreMovimientoEncabezado.class);
        if (preMovimientoEncabezadoPK != null) {
            dto.setCodigoSucursal(preMovimientoEncabezadoPK.getCodigoSucursal());
            dto.setCodigoMovimiento(preMovimientoEncabezadoPK.getCodigoMovimiento());
        }
        if (conTercero != null && preDependencias != null && preTipoMovimiento != null && preUso != null) {
            dto.setCodigoTercero(conTercero.getConTerceroPK().getCodigoTercero());
            dto.setCodigoSecretaria(preDependencias.getPreDependenciasPK().getCodigoDependencia());
            dto.setCodigoTipo(preTipoMovimiento.getPreTipoMovimientoPK().getCodigoTipo());
            dto.setCodigoUso(preUso.getPreUsoPK().getCodigoUso());
        }
        return dto;
    }

    public static PreMovimientoEncabezadoEntidad crearEntidadMovimientoEncabezado(PreMovimientoEncabezado preMovimientoEncabezado) {
        PreMovimientoEncabezadoEntidad entidad = new ModelMapper().map(preMovimientoEncabezado, PreMovimientoEncabezadoEntidad.class);
        entidad.setPreMovimientoEncabezadoPK(new PreMovimientoEncabezadoPK(preMovimientoEncabezado.getCodigoSucursal(), preMovimientoEncabezado.getCodigoMovimiento()));
        entidad.setConTercero(new ConTerceroEntidad(new ConTerceroPK(preMovimientoEncabezado.getCodigoSucursal(), preMovimientoEncabezado.getCodigoTercero())));
        entidad.setPreDependencias(new PreDependenciasEntidad(new PreDependenciasPK(preMovimientoEncabezado.getCodigoSucursal(), preMovimientoEncabezado.getCodigoSecretaria())));
        entidad.setPreTipoMovimiento(new PreTipoMovimientoEntidad(new PreTipoMovimientoPK(preMovimientoEncabezado.getCodigoSucursal(), preMovimientoEncabezado.getCodigoTipo())));
        entidad.setPreUso(new PreUsoEntidad(new PreUsoPK(preMovimientoEncabezado.getCodigoSucursal(), preMovimientoEncabezado.getCodigoUso())));
        return entidad;
    }

}
