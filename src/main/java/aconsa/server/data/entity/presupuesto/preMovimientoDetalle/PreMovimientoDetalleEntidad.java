package aconsa.server.data.entity.presupuesto.preMovimientoDetalle;

import aconsa.server.data.domain.presupuesto.preMovimientoDetalle.PreMovimientoDetalle;
import aconsa.server.data.entity.presupuesto.preMovimientoEncabezado.PreMovimientoEncabezadoEntidad;
import aconsa.server.data.entity.presupuesto.preMovimientoEncabezado.PreMovimientoEncabezadoPK;
import aconsa.server.data.entity.presupuesto.preRecurso.PreRecursoEntidad;
import aconsa.server.data.entity.presupuesto.preRecurso.PreRecursoPK;
import aconsa.server.data.entity.presupuesto.preRubro.PreRubroEntidad;
import aconsa.server.data.entity.presupuesto.preRubro.PreRubroPK;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "p_movidet", indexes = {
        @Index(name = "grupoIndexes1",
                columnList = "cdgscr, cdgmvm, conceptoReferencia, estrgs"),
        @Index(name = "grupoIndexes2",
                columnList = "cdgrbr, cdgrcr, estrgs")})
public class PreMovimientoDetalleEntidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PreMovimientoDetallePK preMovimientoDetallePK;

    @Column(name = "vlrrgs", columnDefinition = "decimal(18,2)")
    private BigDecimal valorRegistro;

    @Column(name = "terceroReferencia", length = 50)
    private String terceroReferencia;

    @Column(name = "conceptoReferencia", length = 50)
    private String conceptoReferencia;

    @Column(name = "estrgs", length = 1)
    private String estadoRegistro;

    @Column(name = "fchcrc", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "fchmdf")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Column(name = "usrmdf")
    private String usuarioModificacion;

    @Column(name = "cdgrbr", columnDefinition = "varchar(50)")
    private String cdgrbr;

    @Column(name = "cdgrcr", columnDefinition = "varchar(50)")
    private String cdgrcr;

    @Column(name = "cdgvgn", columnDefinition = "int(11) default 0")
    private String cdgvgn;

    @ManyToOne()
    @MapsId("preMovimientoDetallePK")
    @JoinColumns(value = {
            @JoinColumn(name = "cdgmvm"),
            @JoinColumn(name = "cdgscr")
    }, foreignKey = @ForeignKey(
            foreignKeyDefinition = "FOREIGN KEY (cdgmvm, cdgscr) " +
                    "REFERENCES p_movienc (cdgmvm, cdgscr) " +
                    "ON UPDATE CASCADE"))
    private PreMovimientoEncabezadoEntidad preMovimientoEncabezado;

    @ManyToOne()
    @MapsId("preMovimientoDetallePK")
    @JoinColumns(value = {
            @JoinColumn(name = "cdgrcr"),
            @JoinColumn(name = "cdgscr"),
            @JoinColumn(name = "cdgvgn"),
    }, foreignKey = @ForeignKey(
            foreignKeyDefinition = "FOREIGN KEY (cdgrcr, cdgscr, cdgvgn) REFERENCES p_recurso (cdgrcr, cdgscr, cdgvgn) ON UPDATE CASCADE"))
    private PreRecursoEntidad preRecurso;

    @ManyToOne()
    @MapsId("preMovimientoDetallePK")
    @JoinColumns(value = {
            @JoinColumn(name = "cdgrbr"),
            @JoinColumn(name = "cdgscr"),
            @JoinColumn(name = "cdgvgn"),
    }, foreignKey = @ForeignKey(
            foreignKeyDefinition = "FOREIGN KEY (cdgrbr, cdgscr, cdgvgn) REFERENCES p_rubro (cdgrbr, cdgscr, cdgvgn) ON UPDATE CASCADE"))
    private PreRubroEntidad preRubro;

    public PreMovimientoDetalle serialize() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setAmbiguityIgnored(true);
        PreMovimientoDetalle dto = mapper.map(this, PreMovimientoDetalle.class);
        if (preMovimientoDetallePK != null) {
            dto.setCodigoSucursal(preMovimientoDetallePK.getCodigoSucursal());
            dto.setCodigoMovimiento(preMovimientoDetallePK.getCodigoMovimiento());
            dto.setCnsrgs(preMovimientoDetallePK.getCnsrgs());
        }
        if (preMovimientoEncabezado != null && preRecurso != null && preRubro != null) {
            dto.setCdgvgn(preRecurso.getPreRecursoPK().getCodigoVigencia());
            dto.setCdgrcr(preRecurso.getPreRecursoPK().getCodigoRecurso());
            dto.setCdgrbr(preRubro.getPreRubroPK().getCodigoRubro());
        }
        return dto;
    }

    public static PreMovimientoDetalleEntidad crearMovimientoDetalleEntidad(PreMovimientoDetalle preMovimientoDetalle) {
        PreMovimientoDetalleEntidad entidad = new ModelMapper().map(preMovimientoDetalle, PreMovimientoDetalleEntidad.class);
        entidad.setPreMovimientoDetallePK(new PreMovimientoDetallePK(
                preMovimientoDetalle.getCodigoSucursal(), preMovimientoDetalle.getCodigoMovimiento(), preMovimientoDetalle.getCnsrgs()
        ));
        entidad.setPreMovimientoEncabezado(new PreMovimientoEncabezadoEntidad(new PreMovimientoEncabezadoPK(
                preMovimientoDetalle.getCodigoSucursal(), preMovimientoDetalle.getCodigoMovimiento()
        )));
        entidad.setPreRecurso(new PreRecursoEntidad(new PreRecursoPK(
                preMovimientoDetalle.getCodigoSucursal(), preMovimientoDetalle.getCdgvgn(), preMovimientoDetalle.getCdgrcr()
        )));
        entidad.setPreRubro(new PreRubroEntidad(new PreRubroPK(
                preMovimientoDetalle.getCodigoSucursal(), preMovimientoDetalle.getCdgvgn(), preMovimientoDetalle.getCdgrbr()
        )));
        return entidad;
    }

}
