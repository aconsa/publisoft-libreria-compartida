package aconsa.server.data.entity.presupuesto.preRubro;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class PreRubroPK implements Serializable {

    @Column(name = "cdgscr", length = 50)
    private String codigoSucursal;

    @Column(name = "cdgvgn")
    private Integer codigoVigencia;

    @Column(name = "cdgrbr", length = 50)
    private String codigoRubro;
}
