package aconsa.server.data.entity.presupuesto.preTipoMovimiento;

import aconsa.server.data.domain.presupuesto.preTipoMovimiento.PreTipoMovimiento;
import aconsa.server.data.entity.presupuesto.preMovimientoEncabezado.PreMovimientoEncabezadoEntidad;
import aconsa.server.data.entity.presupuesto.preTiposMovimientosBases.PreTiposMovimientosBasesEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "p_tipomov", indexes = @Index(columnList = "cdgafc, frmafc, clstpo, esttpo"))
public class PreTipoMovimientoEntidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PreTipoMovimientoPK preTipoMovimientoPK;

    @Column(name = "dsctpo")
    private String descripcionTipo;

    @Column(name = "cdgafc", length = 2)
    private String cdgafc;

    @Column(name = "frmafc")
    private Integer frmafc;

    @Column(name = "clstpo", length = 1)
    private String claseTipo;

    @Column(name = "cdgrfr", length = 50)
    private String codigoReferencia;

    @Column(name = "dtltpo", columnDefinition = "text")
    private String dtltpo;

    @Column(name = "ntapie", columnDefinition = "text")
    private String ntapie;

    @Column(name = "esttpo", length = 1)
    private String estadoTipo;

    @Column(name = "fchcrc", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "fchmdf")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Column(name = "usrmdf")
    private String usuarioModificacion;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "preTipoMovimiento")
    private List<PreTiposMovimientosBasesEntidad> preTiposMovimientosBases;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "preTipoMovimiento1")
    private List<PreTiposMovimientosBasesEntidad> preTiposMovimientosBases1;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "preTipoMovimiento")
    private List<PreMovimientoEncabezadoEntidad> preMovimientoEncabezado;

    public PreTipoMovimientoEntidad(PreTipoMovimientoPK preTipoMovimientoPK) {
        this.preTipoMovimientoPK = preTipoMovimientoPK;
    }

    public PreTipoMovimiento serialize() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setAmbiguityIgnored(true);
        PreTipoMovimiento dto = mapper.map(this, PreTipoMovimiento.class);
        if (preTipoMovimientoPK != null) {
            dto.setCodigoSucursal(preTipoMovimientoPK.getCodigoSucursal());
            dto.setCodigoTipo(preTipoMovimientoPK.getCodigoTipo());
        }
        return dto;
    }

    public static PreTipoMovimientoEntidad crearEntidadTipoMovimiento(PreTipoMovimiento preTipoMovimiento) {
        PreTipoMovimientoEntidad entidad = new ModelMapper().map(preTipoMovimiento, PreTipoMovimientoEntidad.class);
        entidad.setPreTipoMovimientoPK(new PreTipoMovimientoPK(preTipoMovimiento.getCodigoSucursal(), preTipoMovimiento.getCodigoTipo()));
        return entidad;
    }

}
