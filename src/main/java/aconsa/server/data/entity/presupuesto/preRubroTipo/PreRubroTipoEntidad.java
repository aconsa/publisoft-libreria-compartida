package aconsa.server.data.entity.presupuesto.preRubroTipo;

import aconsa.server.data.domain.presupuesto.preRubroTipo.PreRubroTipo;
import aconsa.server.data.entity.presupuesto.preRubroClase.PreRubroClaseEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "p_rubrotipo", indexes = @Index(columnList = "fchcrc"))
public class PreRubroTipoEntidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PreRubroTipoPK preRubroTipoPK;

    @Column(name = "dsctpo")
    private String descripcionTipo;

    @Column(name = "esttpo", length = 1)
    private String estadoTipo;

    @Column(name = "fchcrc", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "fchmdf")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Column(name = "usrmdf")
    private String usuarioModificacion;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "preRubroTipo")
    private List<PreRubroClaseEntidad> preRubroClase;

    public PreRubroTipoEntidad(PreRubroTipoPK preRubroTipoPK) {
        this.preRubroTipoPK = preRubroTipoPK;
    }

    public PreRubroTipo serialize() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setAmbiguityIgnored(true);
        PreRubroTipo dto = mapper.map(this, PreRubroTipo.class);
        if (preRubroTipoPK != null) {
            dto.setCodigoSucursal(preRubroTipoPK.getCodigoSucursal());
            dto.setCodigoTipo(preRubroTipoPK.getCodigoTipo());
        }
        return dto;
    }

    public static PreRubroTipoEntidad crearEntidadRubroTipo(PreRubroTipo preRubroTipo) {
        PreRubroTipoEntidad entidad = new ModelMapper().map(preRubroTipo, PreRubroTipoEntidad.class);
        entidad.setPreRubroTipoPK(new PreRubroTipoPK(preRubroTipo.getCodigoSucursal(), preRubroTipo.getCodigoTipo()));
        return entidad;
    }
}
