package aconsa.server.data.entity.presupuesto.preTiposMovimientosBases;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class PreTiposMovimientosBasesPK implements Serializable {

    @Column(name = "codigoSucursal", length = 50)
    private String codigoSucursal;

    @Column(name = "codigoBase", columnDefinition = "varchar(50) default ''")
    private String codigoBase;

    @Column(name = "codigoTipo", columnDefinition = "varchar(2) default ''")
    private String codigoTipo;

    @Column(name = "codigoAfecta", columnDefinition = "varchar(2) default ''")
    private String codigoAfecta;

}
