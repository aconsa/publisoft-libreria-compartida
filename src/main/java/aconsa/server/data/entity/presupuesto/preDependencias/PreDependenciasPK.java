package aconsa.server.data.entity.presupuesto.preDependencias;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class PreDependenciasPK implements Serializable {

    @Column(name = "codigoSucursal", length = 50)
    private String codigoSucursal;

    @Column(name = "codigoDependencia", length = 50)
    private String codigoDependencia;

}
