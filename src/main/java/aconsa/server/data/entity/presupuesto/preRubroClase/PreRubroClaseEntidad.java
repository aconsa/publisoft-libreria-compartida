package aconsa.server.data.entity.presupuesto.preRubroClase;

import aconsa.server.data.domain.presupuesto.preRubroClase.PreRubroClase;
import aconsa.server.data.entity.presupuesto.preRubro.PreRubroEntidad;
import aconsa.server.data.entity.presupuesto.preRubroTipo.PreRubroTipoEntidad;
import aconsa.server.data.entity.presupuesto.preRubroTipo.PreRubroTipoPK;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "p_rubroclase", indexes = @Index(columnList = "estcls"))
public class PreRubroClaseEntidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PreRubroClasePK preRubroClasePK;

    @Column(name = "dsccls")
    private String descripcionClase;

    @Column(name = "ordenReporte", columnDefinition = "int(11) default 0")
    private Integer ordenReporte;

    @Column(name = "estcls", length = 1)
    private String estadoClase;

    @Column(name = "fchcrc", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "fchmdf")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Column(name = "usrmdf")
    private String usuarioModificacion;

    @ManyToOne()
    @MapsId("preRubroClasePK")
    @JoinColumns(value = {
            @JoinColumn(name = "cdgscr"),
            @JoinColumn(name = "cdgtpo")
    }, foreignKey = @ForeignKey(
            foreignKeyDefinition = "FOREIGN KEY (cdgscr, cdgtpo) " +
                    "REFERENCES p_rubrotipo (cdgscr, cdgtpo) " +
                    "ON UPDATE CASCADE"))
    private PreRubroTipoEntidad preRubroTipo;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "preRubroClase")
    private List<PreRubroEntidad> preRubro;

    public PreRubroClaseEntidad(PreRubroClasePK preRubroClasePK) {
        this.preRubroClasePK = preRubroClasePK;
    }

    public PreRubroClase serialize() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setAmbiguityIgnored(true);
        PreRubroClase dto = mapper.map(this, PreRubroClase.class);
        if (preRubroClasePK != null) {
            dto.setCodigoSucursal(preRubroClasePK.getCodigoSucursal());
            dto.setCodigoTipo(preRubroClasePK.getCodigoTipo());
            dto.setCodigoClase(preRubroClasePK.getCodigoClase());
        }
        if (preRubroTipo != null) {
            dto.setCodigoTipo(preRubroTipo.getPreRubroTipoPK().getCodigoTipo());
        }
        return dto;
    }

    public static PreRubroClaseEntidad crearEntidadRubroClase(PreRubroClase preRubroClase) {
        PreRubroClaseEntidad entidad = new ModelMapper().map(preRubroClase, PreRubroClaseEntidad.class);
        entidad.setPreRubroClasePK(new PreRubroClasePK(preRubroClase.getCodigoSucursal(), preRubroClase.getCodigoTipo(), preRubroClase.getCodigoClase()));
        entidad.setPreRubroTipo(new PreRubroTipoEntidad(new PreRubroTipoPK(preRubroClase.getCodigoSucursal(), preRubroClase.getCodigoTipo())));
        return entidad;
    }
}
