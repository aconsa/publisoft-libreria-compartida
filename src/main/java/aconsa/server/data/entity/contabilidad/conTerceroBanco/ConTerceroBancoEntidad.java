package aconsa.server.data.entity.contabilidad.conTerceroBanco;

import aconsa.server.data.domain.contabilidad.conTerceroBanco.ConTerceroBanco;
import aconsa.server.data.entity.contabilidad.conTercero.ConTerceroEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "c_tercero_banco")
public class ConTerceroBancoEntidad implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ConTerceroBancoPK conTerceroBancoPK;

    @Column(name = "dscbnc")
    private String dscbnc;

    @Column(name = "dscbnc_adicional",
            columnDefinition = "varchar(255) comment 'Usado en plantillas de pago específicas'")
    private String dscbncAdicional;

    @Basic(optional = false)
    @Column(name = "cdgbnc_adicional",
            length = 50, nullable = false,
            columnDefinition = "varchar(50) comment 'Usado en plantillas de pago específicas'")
    private String cdgbncAdicional;

    @Column(name = "estbnc", length = 1, columnDefinition = "char(1) default 'T'")
    private Character estbnc;

    @Column(name = "fchcrc")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "fchmdf")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Column(name = "usrmdf", columnDefinition = "varchar(255) default 'suser_sname()'")
    private String usuarioModificacion;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "conTerceroBanco")
    private List<ConTerceroEntidad> conTercero;

    public ConTerceroBancoEntidad(ConTerceroBancoPK conTerceroBancoPK) {
        this.conTerceroBancoPK = conTerceroBancoPK;
    }

    public ConTerceroBanco serialize() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setAmbiguityIgnored(true);
        ConTerceroBanco dto = mapper.map(this, ConTerceroBanco.class);
        if (conTerceroBancoPK != null) {
            dto.setCodigoSucursal(conTerceroBancoPK.getCodigoSucursal());
            dto.setCodigoBanco(conTerceroBancoPK.getCodigoBanco());
        }
        return dto;
    }

    public static ConTerceroBancoEntidad crearEntidadTerceroBanco(ConTerceroBanco conTerceroBanco) {
        ConTerceroBancoEntidad entidad = new ModelMapper()
                .map(conTerceroBanco, ConTerceroBancoEntidad.class);
        entidad.setConTerceroBancoPK(new ConTerceroBancoPK(
                conTerceroBanco.getCodigoSucursal(), conTerceroBanco.getCodigoBanco()));
        return entidad;
    }
}
