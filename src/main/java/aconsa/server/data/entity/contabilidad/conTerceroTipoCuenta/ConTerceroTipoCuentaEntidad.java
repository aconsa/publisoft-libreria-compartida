package aconsa.server.data.entity.contabilidad.conTerceroTipoCuenta;

import aconsa.server.data.domain.contabilidad.conTerceroTipoCuenta.ConTerceroTipoCuenta;
import aconsa.server.data.entity.contabilidad.conTercero.ConTerceroEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "c_tercero_tipocuenta")
public class ConTerceroTipoCuentaEntidad implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    protected ConTerceroTipoCuentaPK conTerceroTipocuentaPK;

    @Column(name = "dsctpo")
    private String descripcionTipo;

    @Column(name = "esttpo", columnDefinition = "char(1) default 'T'")
    private Character estadoTipo;

    @Column(name = "fchcrc")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "fchmdf")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Column(name = "usrmdf", columnDefinition = "varchar(255) default 'suser_sname()'")
    private String usuarioModificacion;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "conTerceroTipocuenta")
    private List<ConTerceroEntidad> conTerceroLista;

    public ConTerceroTipoCuentaEntidad(ConTerceroTipoCuentaPK conTerceroTipocuentaPK) {
        this.conTerceroTipocuentaPK = conTerceroTipocuentaPK;
    }

    public ConTerceroTipoCuenta serialize() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setAmbiguityIgnored(true);
        ConTerceroTipoCuenta dto = mapper.map(this, ConTerceroTipoCuenta.class);
        if (conTerceroTipocuentaPK != null) {
            dto.setCodigoSucursal(conTerceroTipocuentaPK.getCodigoSucursal());
            dto.setCodigoTipo(conTerceroTipocuentaPK.getCodigoTipo());
        }
        return dto;
    }

    public static ConTerceroTipoCuentaEntidad crearEntidadTerceroTipoCuenta(
            ConTerceroTipoCuenta conTerceroTipoCuenta) {
        ConTerceroTipoCuentaEntidad entidad = new ModelMapper().map(
                conTerceroTipoCuenta, ConTerceroTipoCuentaEntidad.class);
        entidad.setConTerceroTipocuentaPK(new ConTerceroTipoCuentaPK(
                conTerceroTipoCuenta.getCodigoSucursal(), conTerceroTipoCuenta.getCodigoTipo()));

        return entidad;
    }

}
