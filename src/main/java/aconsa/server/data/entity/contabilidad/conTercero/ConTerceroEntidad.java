package aconsa.server.data.entity.contabilidad.conTercero;

import aconsa.server.data.domain.contabilidad.conTercero.ConTercero;
import aconsa.server.data.entity.contabilidad.conTerceroBanco.ConTerceroBancoEntidad;
import aconsa.server.data.entity.contabilidad.conTerceroBanco.ConTerceroBancoPK;
import aconsa.server.data.entity.contabilidad.conTerceroTipoCuenta.ConTerceroTipoCuentaEntidad;
import aconsa.server.data.entity.contabilidad.conTerceroTipoCuenta.ConTerceroTipoCuentaPK;
import aconsa.server.data.entity.generales.genMunicipios.GenMunicipiosEntidad;
import aconsa.server.data.entity.generales.genMunicipios.GenMunicipiosPK;
import aconsa.server.data.entity.generales.genTiposDocumentos.GenTiposDocumentosEntidad;
import aconsa.server.data.entity.generales.genTiposDocumentos.GenTiposDocumentosPK;
import aconsa.server.data.entity.presupuesto.preMovimientoEncabezado.PreMovimientoEncabezadoEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "c_tercero")
public class ConTerceroEntidad implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    protected ConTerceroPK conTerceroPK;

    @Column(name = "dsctrc")
    private String dsctrc;

    @Column(name = "ap1trc")
    private String ap1trc;

    @Column(name = "ap2trc")
    private String ap2trc;

    @Column(name = "grstrc", length = 2)
    private String grstrc;

    @Column(name = "rhgtrc", length = 1)
    private String rhgtrc;

    @Column(name = "ftotrc")
    private String ftotrc;

    @Column(name = "teltrc")
    private String teltrc;

    @Column(name = "faxtrc")
    private String faxtrc;

    @Column(name = "dirtrc")
    private String dirtrc;

    @Column(name = "fchtrc")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fchtrc;

    @Column(name = "sextrc", length = 1)
    private String sextrc;

    @Column(name = "tpoemp", length = 1)
    private String tpoemp;

    @Column(name = "sgltrc", length = 50)
    private String sgltrc;

    @Column(name = "crrelc")
    private String crrelc;

    @Column(name = "cntbnc", columnDefinition = "varchar(255) comment 'número de cuenta'")
    private String cntbnc;

    @Column(name = "clslbr")
    private Integer clslbr;

    @Column(name = "nmrlbr", length = 50)
    private String nmrlbr;

    @Column(name = "dstlbr", length = 50)
    private String dstlbr;

    @Column(name = "ncntrc")
    private String ncntrc;

    @Column(name = "grdbsc")
    private Integer grdbsc;

    @Column(name = "titbsc")
    private String titbsc;

    @Column(name = "fchbsc")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fchbsc;

    @Column(name = "autrtn")
    private Integer autrtn;

    @Column(name = "cdgofc", length = 50)
    private String cdgofc;

    @Column(name = "tpotrc")
    private String tpotrc;

    @Column(name = "esttrc", length = 1)
    private String esttrc;

    @Column(name = "fchcrc", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "fchmdf")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Column(name = "usrmdf")
    private String usuarioModificacion;

    @Column(name = "bnccnt",
            columnDefinition = "varchar(255) NULL comment 'Código Banco donde está la Cuenta Bancaria'")
    private String bnccnt;

    @Column(name = "tpocnt", columnDefinition = "varchar(255) comment 'Código Tipo de Cuenta Bancaria'")
    private String tpocnt;

    @Column(name = "paicrr")
    private String paicrr;

    @Column(name = "dptcrr")
    private String dptcrr;

    @Column(name = "restrc")
    private String restrc;

    @Column(name = "dptncm")
    private String dptncm;

    @Column(name = "munncm")
    private String munncm;

    @Column(name = "paincm")
    private String paincm;

    @Column(name = "tpodcm", columnDefinition = "varchar(2)")
    private String tpodcm;


    @ManyToOne()
    @MapsId("conTerceroPK")
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumns(value = {
            @JoinColumn(name = "bnccnt"),
            @JoinColumn(name = "cdgscr")
    }, foreignKey = @ForeignKey(
            foreignKeyDefinition = "FOREIGN KEY (bnccnt, cdgscr) REFERENCES c_tercero_banco (cdgbnc, cdgscr) ON UPDATE CASCADE"
    ))
    private ConTerceroBancoEntidad conTerceroBanco;

    @ManyToOne()
    @MapsId("conTerceroPK")
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumns(value = {
            @JoinColumn(name = "cdgscr"),
            @JoinColumn(name = "tpocnt")
    }, foreignKey = @ForeignKey(
            foreignKeyDefinition = "FOREIGN KEY (cdgscr, tpocnt) REFERENCES c_tercero_tipocuenta (cdgscr, cdgtpo) ON UPDATE CASCADE"
    ))
    private ConTerceroTipoCuentaEntidad conTerceroTipocuenta;

    @ManyToOne()
    @MapsId("conTerceroPK")
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumns(value = {
            @JoinColumn(name = "dptcrr"),
            @JoinColumn(name = "restrc"),
            @JoinColumn(name = "paicrr"),
            @JoinColumn(name = "cdgscr"),
    }, foreignKey = @ForeignKey(
            foreignKeyDefinition = "FOREIGN KEY (dptcrr, restrc, paicrr, cdgscr) REFERENCES g_municipios(codigoDpto, codigoMunicipio, codigoPais, codigoSucursal) ON UPDATE CASCADE"
    ))
    private GenMunicipiosEntidad genMunicipios;

    @ManyToOne()
    @MapsId("conTerceroPK")
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumns(value = {
            @JoinColumn(name = "dptncm"),
            @JoinColumn(name = "munncm"),
            @JoinColumn(name = "paincm"),
            @JoinColumn(name = "cdgscr")
    }, foreignKey = @ForeignKey(
            foreignKeyDefinition = "FOREIGN KEY (dptncm, munncm, paincm, cdgscr) REFERENCES g_municipios(codigoDpto, codigoMunicipio, codigoPais, codigoSucursal) ON UPDATE CASCADE"
    ))
    private GenMunicipiosEntidad genMunicipios1;

    @ManyToOne()
    @MapsId("conTerceroPK")
    @JoinColumns(value = {
            @JoinColumn(name = "cdgscr"),
            @JoinColumn(name = "tpodcm")
    }, foreignKey = @ForeignKey(
            foreignKeyDefinition = "FOREIGN KEY (cdgscr, tpodcm) REFERENCES g_tiposdocumentos (codigoSucursal, tipoDocumento) ON UPDATE CASCADE"
    ))
    private GenTiposDocumentosEntidad genTiposDocumentos;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "conTercero")
    private List<PreMovimientoEncabezadoEntidad> preMovimientoEncabezado;

    public ConTerceroEntidad(ConTerceroPK conTerceroPK) {
        this.conTerceroPK = conTerceroPK;
    }

    public ConTercero serialize() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setAmbiguityIgnored(true);
        ConTercero dto = mapper.map(this, ConTercero.class);
        if (conTerceroPK != null) {
            dto.setCodigoSucursal(conTerceroPK.getCodigoSucursal());
            dto.setCodigoTercero(conTerceroPK.getCodigoTercero());
        }
        if (conTerceroBanco != null && conTerceroTipocuenta != null && genMunicipios != null &&
                genMunicipios1 != null && genTiposDocumentos != null) {
            dto.setBnccnt(conTerceroBanco.getConTerceroBancoPK().getCodigoBanco());
            dto.setTpocnt(conTerceroTipocuenta.getConTerceroTipocuentaPK().getCodigoTipo());
            dto.setPaicrr(genMunicipios.getGenMunicipiosPK().getCodigoPais());
            dto.setDptcrr(genMunicipios.getGenMunicipiosPK().getCodigoDepartamento());
            dto.setRestrc(genMunicipios.getGenMunicipiosPK().getCodigoMunicipio());
            dto.setPaincm(genMunicipios1.getGenMunicipiosPK().getCodigoPais());
            dto.setDptncm(genMunicipios1.getGenMunicipiosPK().getCodigoDepartamento());
            dto.setMunncm(genMunicipios1.getGenMunicipiosPK().getCodigoMunicipio());
            dto.setTpodcm(genTiposDocumentos.getGenTiposdocumentosPK().getTipoDocumento());
        }
        return dto;
    }

    public static ConTerceroEntidad crearEntidadTercero(ConTercero conTercero) {
        ConTerceroEntidad entidad = new ModelMapper().map(conTercero, ConTerceroEntidad.class);
        entidad.setConTerceroPK(new ConTerceroPK(conTercero.getCodigoSucursal(),
                conTercero.getCodigoTercero()));

        entidad.setConTerceroBanco(new ConTerceroBancoEntidad(new ConTerceroBancoPK(
                conTercero.getCodigoSucursal(), conTercero.getBnccnt())));

        entidad.setConTerceroTipocuenta(new ConTerceroTipoCuentaEntidad(new ConTerceroTipoCuentaPK(
                conTercero.getCodigoSucursal(), conTercero.getTpocnt()
        )));

        entidad.setGenMunicipios(new GenMunicipiosEntidad(new GenMunicipiosPK(
                conTercero.getCodigoSucursal(), conTercero.getPaicrr(),
                conTercero.getDptcrr(), conTercero.getRestrc()
        )));

        entidad.setGenMunicipios1(new GenMunicipiosEntidad(new GenMunicipiosPK(
                conTercero.getCodigoSucursal(), conTercero.getPaincm(),
                conTercero.getDptncm(), conTercero.getMunncm()
        )));

        entidad.setGenTiposDocumentos(new GenTiposDocumentosEntidad(new GenTiposDocumentosPK(
                conTercero.getCodigoSucursal(), conTercero.getTpodcm()
        )));

        return entidad;
    }

}
