package aconsa.server.data.entity.contabilidad.conTerceroTipoCuenta;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class ConTerceroTipoCuentaPK implements Serializable {

    @Column(name = "cdgscr", length = 50)
    private String codigoSucursal;

    @Column(name = "cdgtpo", columnDefinition = "varchar(50) comment 'Tipo de Cuenta (ahorro, Corriente)'")
    private String codigoTipo;
}
