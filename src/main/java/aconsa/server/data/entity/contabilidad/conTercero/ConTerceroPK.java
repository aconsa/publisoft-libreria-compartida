package aconsa.server.data.entity.contabilidad.conTercero;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class ConTerceroPK implements Serializable {

    @Column(name = "cdgscr", columnDefinition = "varchar(50) default ''")
    private String codigoSucursal;

    @Column(name = "cdgtrc", columnDefinition = "varchar(50) default ''")
    private String codigoTercero;

}