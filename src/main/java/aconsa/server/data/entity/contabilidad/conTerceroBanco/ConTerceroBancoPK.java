package aconsa.server.data.entity.contabilidad.conTerceroBanco;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class ConTerceroBancoPK implements Serializable {

    @Column(name = "cdgscr", length = 50)
    private String codigoSucursal;

    @Column(name = "cdgbnc", columnDefinition = "varchar(50) comment 'Código Banco Tercero'")
    private String codigoBanco;
}
