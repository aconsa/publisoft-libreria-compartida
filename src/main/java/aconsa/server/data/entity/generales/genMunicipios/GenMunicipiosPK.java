package aconsa.server.data.entity.generales.genMunicipios;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class GenMunicipiosPK implements Serializable {

    @Column(name = "codigoSucursal", columnDefinition = "varchar(50) default ''")
    private String codigoSucursal;

    @Column(name = "codigoPais", length = 50)
    private String codigoPais;

    @Column(name = "codigoDpto", columnDefinition = "varchar(50) default ''")
    private String codigoDepartamento;

    @Column(name = "codigoMunicipio", columnDefinition = "varchar(50) default ''")
    private String codigoMunicipio;

}
