package aconsa.server.data.entity.generales.genDepartamentos;

import aconsa.server.data.domain.generales.genDepartamentos.GenDepartamentos;
import aconsa.server.data.entity.generales.genMunicipios.GenMunicipiosEntidad;
import aconsa.server.data.entity.generales.genPaises.GenPaisesEntidad;
import aconsa.server.data.entity.generales.genPaises.GenPaisesPK;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "g_departamentos", indexes = {@Index(columnList = "estadoDpto")})
public class GenDepartamentosEntidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GenDepartamentosPK genDepartamentosPK;

    @Column(name = "descripcionDpto")
    private String descripcionDepartamento;

    @Column(name = "estadoDpto", length = 1)
    private String estadoDepartamento;

    @Column(name = "fechaCreacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "usuarioCreacion")
    private String usuarioCreacion;

    @Column(name = "fechaModificacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Column(name = "usuarioModificacion")
    private String usuarioModificacion;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "genDepartamentos")
    private List<GenMunicipiosEntidad> genMunicipios;

    @ManyToOne()
    @MapsId("genDepartamentosPK")
    @JoinColumns(value = {
            @JoinColumn(name = "codigoPais"),
            @JoinColumn(name = "codigoSucursal")
    }, foreignKey = @ForeignKey(
            foreignKeyDefinition = "FOREIGN KEY (codigoPais, codigoSucursal) " +
                    "REFERENCES g_paises (codigoPais, codigoSucursal) " +
                    "ON UPDATE CASCADE"))
    private GenPaisesEntidad genPaises;

    public GenDepartamentosEntidad(GenDepartamentosPK genDepartamentosPK) {
        this.genDepartamentosPK = genDepartamentosPK;
    }

    public GenDepartamentos serialize() {
        GenDepartamentos dto = new ModelMapper().map(this, GenDepartamentos.class);
        if (genDepartamentosPK != null) {
            dto.setCodigoDepartamento(genDepartamentosPK.getCodigoDepartamento());
            dto.setCodigoPais(genDepartamentosPK.getCodigoPais());
            dto.setCodigoSucursal(genDepartamentosPK.getCodigoSucursal());
        }
        return dto;
    }

    public static GenDepartamentosEntidad crearGenDepartamentosEntidad(GenDepartamentos genDepartamentos) {
        GenDepartamentosEntidad entidad = new ModelMapper().map(
                genDepartamentos, GenDepartamentosEntidad.class);
        entidad.setGenDepartamentosPK(new GenDepartamentosPK(
                genDepartamentos.getCodigoDepartamento(),
                genDepartamentos.getCodigoPais(), genDepartamentos.getCodigoSucursal()));

        entidad.setGenPaises(new GenPaisesEntidad(new GenPaisesPK(
                genDepartamentos.getCodigoPais(), genDepartamentos.getCodigoSucursal()
        )));
        return entidad;
    }

}
