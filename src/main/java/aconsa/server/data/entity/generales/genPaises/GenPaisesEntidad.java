package aconsa.server.data.entity.generales.genPaises;

import aconsa.server.data.domain.generales.genPaises.GenPaises;
import aconsa.server.data.entity.generales.genDepartamentos.GenDepartamentosEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "g_paises", indexes = {@Index(columnList = "estadoPais")})
public class GenPaisesEntidad implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    protected GenPaisesPK genPaisesPK;

    @Column(name = "descripcionPais")
    private String descripcionPais;

    @Column(name = "estadoPais", length = 1)
    private String estadoPais;

    @Column(name = "fechaCreacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "usuarioCreacion")
    private String usuarioCreacion;

    @Column(name = "fechaModificacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Column(name = "usuarioModificacion")
    private String usuarioModificacion;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "genPaises")
    private List<GenDepartamentosEntidad> genDepartamentos;

    public GenPaisesEntidad(GenPaisesPK genPaisesPK) {
        this.genPaisesPK = genPaisesPK;
    }

    public GenPaises serialize() {
        GenPaises dto = new ModelMapper().map(this, GenPaises.class);
        if (genPaisesPK != null) {
            dto.setCodigoSucursal(genPaisesPK.getCodigoSucursal());
            dto.setCodigoPais(genPaisesPK.getCodigoPais());
        }
        return dto;
    }

    public static GenPaisesEntidad crearGenPaisesEntidad(GenPaises genPaises) {
        GenPaisesEntidad entidad = new ModelMapper().map(genPaises, GenPaisesEntidad.class);
        entidad.setGenPaisesPK(new GenPaisesPK(
                genPaises.getCodigoPais(), genPaises.getCodigoSucursal())
        );
        return entidad;
    }

}
