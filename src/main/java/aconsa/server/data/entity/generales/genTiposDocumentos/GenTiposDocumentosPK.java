package aconsa.server.data.entity.generales.genTiposDocumentos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class GenTiposDocumentosPK implements Serializable {

    @Column(name = "codigoSucursal", columnDefinition = "varchar(50) default ''")
    private String codigoSucursal;

    @Column(name = "tipoDocumento", columnDefinition = "varchar(50) default ''")
    private String tipoDocumento;

}
