package aconsa.server.data.entity.generales.genPaises;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class GenPaisesPK implements Serializable {

    @Column(name = "codigoPais", columnDefinition = "varchar(50) default ''")
    private String codigoPais;

    @Column(name = "codigoSucursal", columnDefinition = "varchar(50) default ''")
    private String codigoSucursal;


}
