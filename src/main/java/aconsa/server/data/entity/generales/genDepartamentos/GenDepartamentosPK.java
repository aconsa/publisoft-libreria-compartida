package aconsa.server.data.entity.generales.genDepartamentos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class GenDepartamentosPK implements Serializable {

    @Column(name = "codigoDpto", length = 50)
    private String codigoDepartamento;

    @Column(name = "codigoPais", length = 50)
    private String codigoPais;

    @Column(name = "codigoSucursal", length = 50)
    private String codigoSucursal;

}
