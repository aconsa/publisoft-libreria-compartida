package aconsa.server.data.entity.generales.genMunicipios;

import aconsa.server.data.domain.generales.genMunicipios.GenMunicipios;
import aconsa.server.data.entity.contabilidad.conTercero.ConTerceroEntidad;
import aconsa.server.data.entity.generales.genDepartamentos.GenDepartamentosEntidad;
import aconsa.server.data.entity.generales.genDepartamentos.GenDepartamentosPK;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "g_municipios", indexes = {@Index(columnList = "estadoMunicipio")})
public class GenMunicipiosEntidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GenMunicipiosPK genMunicipiosPK;

    @Column(name = "descripcionMunicipio")
    private String descripcionMunicipio;

    @Column(name = "estadoMunicipio", length = 1)
    private String estadoMunicipio;

    @Column(name = "fechaCreacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "usuarioCreacion")
    private String usuarioCreacion;

    @Column(name = "fechaModificacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Column(name = "usuarioModificacion")
    private String usuarioModificacion;

    @ManyToOne()
    @MapsId("genMunicipiosPK")
    @JoinColumns(value = {
            @JoinColumn(name = "codigoDpto"),
            @JoinColumn(name = "codigoPais"),
            @JoinColumn(name = "codigoSucursal"),
    }, foreignKey = @ForeignKey(
            foreignKeyDefinition = "FOREIGN KEY (codigoDpto, codigoPais, codigoSucursal) " +
                    "REFERENCES g_departamentos (codigoDpto, codigoPais, codigoSucursal) " +
                    "ON UPDATE CASCADE"))
    private GenDepartamentosEntidad genDepartamentos;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "genMunicipios")
    private List<ConTerceroEntidad> conTercero;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "genMunicipios1")
    private List<ConTerceroEntidad> conTercero1;

    public GenMunicipiosEntidad(GenMunicipiosPK genMunicipiosPK) {
        this.genMunicipiosPK = genMunicipiosPK;
    }

    public GenMunicipios serialize() {
        GenMunicipios dto = new ModelMapper().map(this, GenMunicipios.class);
        if (genMunicipiosPK != null) {
            dto.setCodigoSucursal(genMunicipiosPK.getCodigoSucursal());
            dto.setCodigoPais(genMunicipiosPK.getCodigoPais());
            dto.setCodigoDepartamento(genMunicipiosPK.getCodigoDepartamento());
            dto.setCodigoMunicipio(genMunicipiosPK.getCodigoMunicipio());
        }
        return dto;
    }

    public static GenMunicipiosEntidad crearGenMunicipiosEntidad(GenMunicipios genMunicipios) {

        GenMunicipiosEntidad entidad = new ModelMapper().map(genMunicipios, GenMunicipiosEntidad.class);
        entidad.setGenMunicipiosPK(new GenMunicipiosPK(
                genMunicipios.getCodigoSucursal(), genMunicipios.getCodigoPais(),
                genMunicipios.getCodigoDepartamento(), genMunicipios.getCodigoMunicipio()
        ));
        entidad.setGenDepartamentos(new GenDepartamentosEntidad(new GenDepartamentosPK(
                genMunicipios.getCodigoDepartamento(), genMunicipios.getCodigoPais(), genMunicipios.getCodigoSucursal()
        )));

        return entidad;
    }
}
