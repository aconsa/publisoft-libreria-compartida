package aconsa.server.data.entity.generales.genTiposDocumentos;

import aconsa.server.data.domain.generales.genTipoDocumento.GenTipoDocumento;
import aconsa.server.data.entity.contabilidad.conTercero.ConTerceroEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "g_tiposdocumentos", indexes = @Index(columnList = "estadoTipo"))
public class GenTiposDocumentosEntidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GenTiposDocumentosPK genTiposdocumentosPK;

    @Column(name = "descripcionTipo")
    private String descripcionTipo;

    @Column(name = "estadoTipo", length = 1)
    private String estadoTipo;

    @Column(name = "fechaCreacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Column(name = "usuarioCreacion")
    private String usuarioCreacion;

    @Column(name = "fechaModificacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Column(name = "usuarioModificacion")
    private String usuarioModificacion;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "genTiposDocumentos")
    private List<ConTerceroEntidad> conTercero;

    public GenTiposDocumentosEntidad(GenTiposDocumentosPK genTiposdocumentosPK) {
        this.genTiposdocumentosPK = genTiposdocumentosPK;
    }

    public GenTipoDocumento serialize() {
        GenTipoDocumento dto = new ModelMapper().map(this, GenTipoDocumento.class);
        if (genTiposdocumentosPK != null) {
            dto.setCodigoSucursal(genTiposdocumentosPK.getCodigoSucursal());
            dto.setTipoDocumento(genTiposdocumentosPK.getTipoDocumento());
        }
        return dto;
    }

    public static GenTiposDocumentosEntidad crearTipoDocumentoEntidad(
            GenTipoDocumento genTipoDocumento) {
        GenTiposDocumentosEntidad entidad = new ModelMapper().map(genTipoDocumento,
                GenTiposDocumentosEntidad.class);
        entidad.setGenTiposdocumentosPK(new GenTiposDocumentosPK(
                genTipoDocumento.getCodigoSucursal(), genTipoDocumento.getTipoDocumento()
        ));
        return entidad;
    }

}
